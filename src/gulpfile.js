var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('scss', function() {
    gulp.src('scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('../dist/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
        './js/jquery.min.js',
        './js/jquery.bxslider.min.js',
        './js/dist.js',
    ])
        .pipe(concat('dist.min.js'))
        .pipe(gulp.dest('../dist/js'));
});

gulp.task('watch', function() {
    gulp.watch('scss/**/*.scss',['scss']);

});