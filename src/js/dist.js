$(function() {

  $.ajax({
    type: 'GET',
    dataType: 'json',
    async: false,
    url: 'https://gist.githubusercontent.com/roblos12/151417b9d5bcd444a2351b6d58db3fdc/raw/acc7ed077a9eb0cba0651bf9759d7b58cacc83c3/data.json',
    success:function(response){
      response.carouselData.forEach(function(v,i) {
        makeItem(v);
      });
    },
    error: function(err) {
      console.error('bad request' + err.responseText);
    }
  });

  function makeItem(props) {
    let {url, productImageAltText, productImageUrl, typeName, name, salesUnit, price} = props;
    const target = $('.dist-container > ul');
    productImageUrl = productImageUrl.toString().split(' ').join('%20');

    switch(price.currency) {
      case "GBP": 
        price.currency = "&pound;";
        break;

      default:
        price.currency = price.currency;
    }

    const slider = '<li class="slider-item">' +
    '<div class="slider-item__img">' + 
    '<a href="'+url+'" title="'+productImageAltText+'">' +
     '<img width="60" height="34" src="'+productImageUrl+'" alt="'+productImageAltText+'"/>' +
     '</a>' +
    '</div>' + 
    '<div class="slider-item__content">' +
       '<h1><a href="'+url+'" title="'+typeName +'">'+name+'</a></h1>' +
       '<span class="price">'+ price.currency + price.formattedValue +'</span>' +
       '<span>'+salesUnit+'</span>' +
       '<div class="slider-item__content__add-to-cart">' +
       '<a href="#" title="Add To Cart">Add to Cart</a>' +
       '</div>' +
    '</div>' + 
  '</li>';

  target.append(slider);
  }

  const width = $(window).outerWidth();
  let slideNum_min = null;
  let slideNum_max = null;

  if(width < 560) {
    slideNum_min = 1;
    slideNum_max = 1;
  }
  else if(width < 1024) {
    slideNum_min = 2;
    slideNum_max = 2;
  }
  else if(width < 1200) {
    slideNum_min = 2;
    slideNum_max = 3;
  }
  else {
    slideNum_min = 4;
    slideNum_max = 5;
  }

  const target = $('.dist-container > ul');
  target.bxSlider({
    mode: 'horizontal',
    slideMargin: 10,
    minSlides: slideNum_min,
    maxSlides: slideNum_max,
    slideWidth: 250,
    controls: (width < 1200) ? false : true,
    auto: true
  });

});